package de.fjobilabs.compoundinterestcalculator;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 28.10.2017 - 00:28:12
 */
public class CompoundInterestCalculator {
    
    public TimeCalculationResult calcTimeForTargetAmount(double startAmount, double interestRate,
            double targetAmount) {
        /*
         * TODO Loop does not terminate if startAmount is 0 or interestRate is
         * negative
         */
        int years = 0;
        double actuallySavedAmount = startAmount;
        while (actuallySavedAmount < targetAmount) {
            years++;
            actuallySavedAmount += actuallySavedAmount * interestRate;
        }
        return new TimeCalculationResult(years, actuallySavedAmount);
    }
    
    public double calcSavedAmount(double startAmount, double interestRate, int years) {
        double savedAmount = startAmount;
        for (int i = 0; i < years; i++) {
            savedAmount += savedAmount * interestRate;
        }
        return savedAmount;
    }
}
