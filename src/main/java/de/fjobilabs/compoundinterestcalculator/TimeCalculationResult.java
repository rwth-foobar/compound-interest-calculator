package de.fjobilabs.compoundinterestcalculator;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 28.10.2017 - 00:22:07
 */
public class TimeCalculationResult {
    
    private int years;
    private double actuallySavedAmount;
    
    public TimeCalculationResult(int years, double actuallySavedAmount) {
        this.years = years;
        this.actuallySavedAmount = actuallySavedAmount;
    }
    
    public int getYears() {
        return years;
    }
    
    public double getActuallySavedAmount() {
        return actuallySavedAmount;
    }
}
