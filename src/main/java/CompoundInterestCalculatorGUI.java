
/*
 * Bad practice! SimpleIO uses default package, so we have to use it too. :(
 */
import java.text.MessageFormat;
import java.util.ResourceBundle;

import de.fjobilabs.compoundinterestcalculator.CompoundInterestCalculator;
import de.fjobilabs.compoundinterestcalculator.TimeCalculationResult;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 27.10.2017 - 22:50:27
 */
public class CompoundInterestCalculatorGUI {
    
    private CompoundInterestCalculator calculator;
    private ResourceBundle messages;
    
    public CompoundInterestCalculatorGUI(CompoundInterestCalculator calculator, ResourceBundle messages) {
        this.calculator = calculator;
        this.messages = messages;
    }
    
    public void run() {
        double startAmount = SimpleIO.getDouble(this.messages.getString("startAmount.input.message"));
        double interestRate = SimpleIO.getDouble(this.messages.getString("interestRate.input.message"));
        CalculationOption calculationOption = getCalculationOption();
        
        switch (calculationOption) {
        case Target:
            handleTargetOption(startAmount, interestRate);
            break;
        case Time:
            handleTimeOption(startAmount, interestRate);
            break;
        case Invalid:
        default:
            showMessage("calculationOption.invalid.message", "calculationOption.invalid.title");
        }
    }
    
    private CalculationOption getCalculationOption() {
        String optionString = SimpleIO
                .getString(this.messages.getString("calculationOption.input.message"));
        String targetOptionName = this.messages.getString("calculationOption.target.name");
        String timeOptionName = this.messages.getString("calculationOption.time.name");
        if (targetOptionName.equals(optionString)) {
            return CalculationOption.Target;
        }
        if (timeOptionName.equals(optionString)) {
            return CalculationOption.Time;
        }
        return CalculationOption.Invalid;
    }
    
    private void handleTargetOption(double startAmount, double interestRate) {
        double targetAmount = SimpleIO.getDouble(this.messages.getString("targetAmount.input.message"));
        TimeCalculationResult result = this.calculator.calcTimeForTargetAmount(startAmount, interestRate,
                targetAmount);
        showMessage("calculationOption.target.result.message", "calculationOption.target.result.title",
                result.getYears(), interestRate, startAmount, targetAmount, result.getActuallySavedAmount());
    }
    
    private void handleTimeOption(double startAmount, double interestRate) {
        int years = SimpleIO.getInt(this.messages.getString("years.input.message"));
        double savedAmount = this.calculator.calcSavedAmount(startAmount, interestRate, years);
        showMessage("calculationOption.time.result.message", "calculationOption.time.result.title",
                interestRate, startAmount, years, savedAmount);
    }
    
    private void showMessage(String messageKey, String titleKey, Object... arguments) {
        String pattern = this.messages.getString(messageKey);
        String message = MessageFormat.format(pattern, arguments);
        SimpleIO.output(message, titleKey);
    }
    
    private static enum CalculationOption {
        Target, Time, Invalid
    }
}
