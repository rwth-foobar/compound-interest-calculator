
/*
 * Bad practice! SimpleIO uses default package, so we have to use it too. :(
 */
import java.text.MessageFormat;
import java.util.ResourceBundle;

import de.fjobilabs.compoundinterestcalculator.CompoundInterestCalculator;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 28.10.2017 - 11:28:34
 */
public class CompoundInterestCalculatorLauncher {
    
    private static final String RESOURCE_BUNDLE_NAME = "messages";
    
    public static void main(String[] args) {
        CompoundInterestCalculator calculator = new CompoundInterestCalculator();
        ResourceBundle messages = ResourceBundle.getBundle(RESOURCE_BUNDLE_NAME);
        CompoundInterestCalculatorGUI gui = new CompoundInterestCalculatorGUI(calculator, messages);
        try {
            gui.run();
        } catch (Exception e) {
            e.printStackTrace();
            showErrorMessage(messages, e);
        }
    }
    
    private static void showErrorMessage(ResourceBundle messages, Exception exception) {
        String pattern = messages.getString("unknownError.message");
        String errorMessage = MessageFormat.format(pattern, exception.getLocalizedMessage());
        SimpleIO.output(errorMessage, messages.getString("unknownError.title"));
    }
}
